# AWS EC2 Instance Terraform Outputs
# Public EC2 Instances - Bastion Host

## ec2_bastion_public_instance_ids
output "public_instance_ids" {
  description = "List of IDs of instances"
  value       = module.ec2_public.id
}

## ec2_bastion_public_ip
output "ec2_eip" {
  description = "Elastic IP associated to the Bastion Host"
  value       = resource.aws_eip.bastion_eip.public_ip
}