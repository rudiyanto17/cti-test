variable "vpc_name" {
  type        = string
  default     = "CTI-test"
  description = "variable of vpc"
}


variable "vpc_cidr_block" {

}

variable "vpc_public_subnets" {

}

variable "vpc_private_subnets" {

}

variable "instance_type" {
  description = "EC2 Instance Type"
  type = string
  default = "t2.micro"  
}

# AWS EC2 Instance Key Pair
variable "instance_keypair" {
  description = "AWS EC2 Key pair that need to be associated with EC2 Instance"
  type = string
  default = "rudi-test-key"
}

variable  "instance_name" {
  description = "for instance name or tags"
  type = string
  default = "cti"

}

variable "ami" {
  description = "customized ami"
  type = string 
  default = "ami-05ca0fed4054e24dc"
}

variable "subnet_id" {
  description = "subnet id from vpc"
  type = string
  default = "subnet-02e202f61c6b35866"
}

variable "vpc_id" {
  description = "vpc id"
  type = string
  default = "vpc-0539141989e79124d"
}