---
title: RUDIYANTO CTI test
description: This repo is only for testing purposes
---

## Step-00: Introduction 
1. Create kind cluster
2. deploy metallb
3. set up ingress-nginx controller
4. set up nfs server
5. deploy nginx
6. deploy wordpress 
7. deploy inggress resource
8. set up gitlab ci

## Step-1: Install and create cluster local using kinD.
- **Create your CLUSTER kubernetes:** 
```t
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.14.0/kind-linux-amd64
chmod +x ./kind
sudo mv ./kind /usr/local/bin/kind

``` 

## Step-2: deploy metallb
- **clone this repo first and follow along:** 
```t
cd metallb
kubectl create -f namespace.yaml
kubectl create -f metallb.yaml
kubectl create -f configmap-metallb.yaml
```

## Step-3: Set up nginx-ingress-controll  
```t
cd ../ingress-controller
kubectl create -f deploy.yaml
```

## Step-4: Set up nfs on ubuntu server
```t
sudo apt update
sudo apt install nfs-kernel-server
sudo mkdir /srv/nfs/kubedata
# check the directory
ls -la /var/nfs/general
sudo chown nobody:nogroup /srv/nfs/kubedata
sudo vim /etc/exports

```
- **and fill similar like this:**
```t
# /etc/exports: the access control list for filesystems which may be exported
#		to NFS clients.  See exports(5).
#
# Example for NFSv2 and NFSv3:
# /srv/homes       hostname1(rw,sync,no_subtree_check) hostname2(ro,sync,no_subtree_check)
#
# Example for NFSv4:
# /srv/nfs4        gss/krb5i(rw,sync,fsid=0,crossmnt,no_subtree_check)
# /srv/nfs4/homes  gss/krb5i(rw,sync,no_subtree_check)
#
 /srv/nfs/kubedata	*(rw,sync,no_subtree_check,no_root_squash,no_all_squash,insecure)
```
```t
sudo exports -rav
```
- **set up on the client:**
```t
sudo apt update
sudo apt install nfs-common
# check if you can mount or not
mount -t nfs 3.0.49.249:/srv/nfs/kubedata /mnt
mount | grep kubedata
# and then unmount it
umount
cd ../nfs
kubectl create -f rbac.yaml
kubectl create -f class.yaml
kubectl create -f deployment.yaml
kubectl create -f pvc-nfs.yaml


```

## Step-5: Deploy nginx
```t
cd ../nginx
kubectl create -f nginx-deploy.yaml
kubectl expose deploy nginx-deploy-main --port 80
```

## Step-6: Deploy wordpress

```t
cd ../ 

cat <<EOF >./kustomization.yaml
secretGenerator:
- name: mysql-pass
  literals:
  - password=YOUR_PASSWORD
EOF

cat <<EOF >>./kustomization.yaml
resources:
  - mysql-deployment.yaml
  - wordpress-deployment.yaml
EOF

kubectl apply -k ./
```

## Step-7: Deploy ingress resource
```t
cd ../ingress-resource
kubectl create -f ingress-resource.yaml

```
- **test your ingress:**
```t
sudo vim /etc/hosts
172.18.255.200	nginx.example.com wordpress.example.com

```
**try access the domain example**

## Step-8: Git lab ci
1. add variable on gitlab setting > CICD > variables
2. add variables for CI_REGISTRY_IMAGE, CI_REGISTRY_PASSWORD, and CI_REGISTRY_USER 
4. register the gitlab-runner token 
   ```t
   sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "GR1348941i1uoDVz2ux9npq4YdgzY" \
  --executor "shell" \
  --tag-list "shell, amazon-linux"
   ```
3. edit some file and push to git repository.



